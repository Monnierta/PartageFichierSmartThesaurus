﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class pour afficher nombre de le nombre de mot dans une fenetre
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Thésaurus;

namespace Projet_Thésaurus
{
    public partial class ViewWord : Form
    {
        TemplateFile file;

        /// <summary>
        /// Constructeur de base 
        /// </summary>
        /// <param name="wordCounter">dicto de mot a afficher</param>
        public ViewWord()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Affiche list des mot du fichier
        /// </summary>
        /// <param name="wordCounter"></param>
        public void ShowWord(TemplateFile file)
        {
            this.file = file;
            foreach (var str in file.wordDico)
            {
                ListingWord.Text += (str.Key + " : " + str.Value + "\n");
            }
        }

        private void btn_AccesFile_Click(object sender, EventArgs e)
        {
            file.Open(file);
        }
    }
}

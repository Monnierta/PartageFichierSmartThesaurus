﻿namespace Projet_Thésaurus
{
    partial class ViewFile
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewFile));
            this.pnl_ShowFile = new System.Windows.Forms.Panel();
            this.searchBarreWord = new System.Windows.Forms.TextBox();
            this.searchWord = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeRepertoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serachOnWebsiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_ShowFile
            // 
            this.pnl_ShowFile.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pnl_ShowFile.AutoScroll = true;
            this.pnl_ShowFile.BackColor = System.Drawing.Color.Transparent;
            this.pnl_ShowFile.Location = new System.Drawing.Point(12, 76);
            this.pnl_ShowFile.Name = "pnl_ShowFile";
            this.pnl_ShowFile.Size = new System.Drawing.Size(1309, 672);
            this.pnl_ShowFile.TabIndex = 2;
            // 
            // searchBarreWord
            // 
            this.searchBarreWord.Location = new System.Drawing.Point(412, 50);
            this.searchBarreWord.Name = "searchBarreWord";
            this.searchBarreWord.Size = new System.Drawing.Size(395, 20);
            this.searchBarreWord.TabIndex = 5;
            // 
            // searchWord
            // 
            this.searchWord.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.searchWord.Location = new System.Drawing.Point(813, 50);
            this.searchWord.Name = "searchWord";
            this.searchWord.Size = new System.Drawing.Size(75, 20);
            this.searchWord.TabIndex = 7;
            this.searchWord.Text = "Search";
            this.toolTip1.SetToolTip(this.searchWord, "Button to execut your researche");
            this.searchWord.UseVisualStyleBackColor = true;
            this.searchWord.Click += new System.EventHandler(this.SearchWord_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 50;
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 50;
            this.toolTip1.ReshowDelay = 10;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkMagenta;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.homeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1333, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshToolStripMenuItem,
            this.changeRepertoryToolStripMenuItem,
            this.serachOnWebsiteToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // changeRepertoryToolStripMenuItem
            // 
            this.changeRepertoryToolStripMenuItem.Name = "changeRepertoryToolStripMenuItem";
            this.changeRepertoryToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.changeRepertoryToolStripMenuItem.Text = "Change repertory";
            this.changeRepertoryToolStripMenuItem.Click += new System.EventHandler(this.changeRepertoryToolStripMenuItem_Click);
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.homeToolStripMenuItem.Text = "Help";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.helpToolStripMenuItem.Text = "How it works";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // serachOnWebsiteToolStripMenuItem
            // 
            this.serachOnWebsiteToolStripMenuItem.Name = "serachOnWebsiteToolStripMenuItem";
            this.serachOnWebsiteToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.serachOnWebsiteToolStripMenuItem.Text = "Serach on website";
            this.serachOnWebsiteToolStripMenuItem.Click += new System.EventHandler(this.serachOnWebsiteToolStripMenuItem_Click);
            // 
            // ViewFile
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1333, 760);
            this.Controls.Add(this.searchWord);
            this.Controls.Add(this.searchBarreWord);
            this.Controls.Add(this.pnl_ShowFile);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ViewFile";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = " Smart Thésaurus";
            this.Load += new System.EventHandler(this.ViewFile_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel pnl_ShowFile;
        private System.Windows.Forms.TextBox searchBarreWord;
        private System.Windows.Forms.Button searchWord;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeRepertoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serachOnWebsiteToolStripMenuItem;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using word = Microsoft.Office.Interop.Word;
using System.Net;

using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;

namespace REALSmartThésaurus
{
    class GetTextFromWord
    {
        static Application word = new Application();
        private string[] _wordPerPage;

        private string _link;

        public GetTextFromWord(string[] wordPerPage, string link )
        {
            this.Link = link;
            this.WordPerPage = wordPerPage;
        }

        public string Link { get => _link; set => _link = value; }


        public string[] WordPerPage { get => _wordPerPage; set => _wordPerPage = value; }


        public string[] GetTextInHtml(string link)
        {
            string[] wordArray;
            string allText;
           

           object file = link;

            object nullobj = System.Reflection.Missing.Value;

            Document document = word.Documents.Open(ref file, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj);

            document.ActiveWindow.Selection.WholeStory();

            document.ActiveWindow.Selection.Copy();

            allText = document.Content.Text;

            wordArray = allText.Split(' ');

            document.Close();


            return wordArray;
        }
    }
}

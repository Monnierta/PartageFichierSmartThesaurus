﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class faisant parite de template type fichier
using System;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Class de fichier non lisible
    /// </summary>
    public class UnknowFile : TemplateFile
    {
        /// <summary>
        /// Méthode pour ouvrir le contenu d'un fichier
        /// </summary>
        /// <param name="file"></param>
        public override void Open(TemplateFile file)
        {
            //Cannont be open
        }

        /// <summary>
        /// Retourn le nombres de mot du fichier / dans ce cas un message d'erreur
        /// </summary>
        /// <param name="file"></param>
        public override void ReturnWord(TemplateFile file)
        {
            AllWords[0] = "Non reconnu Impossible a lire";
        }
    }
}

﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Controler principal du program
using Projet_Thésaurus;
using REALSmartThésaurus;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;


namespace Thésaurus
{
    /// <summary>
    /// Class servant a controler tous les elements touchant au fichier du répértoire de recherche
    /// </summary>
    public class ControlerFile
    {
        // Variable init
        private string repertoryAcces = @"K:\INF\Eleves\Classes\CID 17-19\041_JMART";
        private ViewFile view;
        private TemplateFile modelfile;
        private ModelWeb modelWeb;
        


        static string startOfLink = "https://www.etml.ch";
        static GetLink getLink = new GetLink();
        static List<string> linkGone = new List<string>();
        static GetTextFromWord page;
        static List<GetTextFromWord> pageList = new List<GetTextFromWord>();

        static string[] tabWord;
        //private Dictionary<string, string> wordKeeper = new Dictionary<string, string>();
        //private List<string> wordCounter = new List<string>();
        //public  static List<TemplateFile> currentFiles = new List<TemplateFile>();
        public static List<TemplateFile> fileContainWord = new List<TemplateFile>();
        public static TemplateFile[] fileContainWordSorted;

        // Init de type de fichier possible
        Word word = new Word();
        Excel excel = new Excel();
        PowerPoint powerPoint = new PowerPoint();
        Txt txt = new Txt();
        PDF pdf = new PDF();

        //Getter Setter
        public ViewFile View
        {
            get { return view; }
            set { view = value; }
        }
        public TemplateFile Model
        {
            get { return modelfile; }
            set { modelfile = value; }
        }

        public string RepertoryAcces { get => repertoryAcces; set => repertoryAcces = value; }
        internal ModelWeb ModelWeb { get => modelWeb; set => modelWeb = value; }

        //Regex
        const string REGEX_WORD = @"^.*\.docx$";
        const string REGEX_PP = @"^.*\.pptx$";
        const string REGEX_EXCEL = @"^.*\.xlsx$";
        const  string REGEX_TXT = @"^.*\.txt$";
        const string REGEX_PDF = @"^.*\.pdf$";

        /// <summary>
        ///  Méthode comptant nombre de mot
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string CountWord(TemplateFile file)
        {
            return "Truc";
            ModelWeb.getInstance();
        }
        public void GetAllWordWeb()
        {
            linkGone = getLink.getLinks(startOfLink);

            foreach (string link in linkGone)
            {


                page = new GetTextFromWord(tabWord, startOfLink + link);
                page.WordPerPage = (page.GetTextInHtml(page.Link));
                pageList.Add(page);
            }

            foreach (GetTextFromWord Page in pageList)
            {
                foreach (string wordArray in Page.WordPerPage)
                {
                    

                }
            }
        }
        /// <summary>
        /// Obtient tous les fichiers d'un endroit selon un pattern
        /// </summary>
        /// <param name="search">pattern rechercher</param>
        /// <param name="path">Chemin dans le quel on regarde</param>
        public void GetAllFiles()
        {
            var searchDirectory = new System.IO.DirectoryInfo(repertoryAcces);
            FileInfo[] allFilesFromFolder = searchDirectory.GetFiles("*",SearchOption.AllDirectories);
            //Boucle pour créée est ajouter les fichier a la liste
            foreach (FileInfo temp in allFilesFromFolder)
            {
                string type = System.IO.Path.GetExtension(temp.Extension);
                if (Regex.IsMatch(type, REGEX_WORD))
                {
                    TemplateFile test = new Word();
                    test.Path = temp.DirectoryName;
                    test.Name = temp.Name;
                    test.Type = type;
                    test.DisplayWord(test);
                    TemplateFile.AddElement(test);
                }
                else if (Regex.IsMatch(type, REGEX_EXCEL))
                {
                    TemplateFile test = new Excel();
                    test.Path = temp.DirectoryName;
                    test.Name = temp.Name;
                    test.Type = type;
                    test.DisplayWord(test);
                    TemplateFile.AddElement(test);
                }
                else if (Regex.IsMatch(type, REGEX_PP))
                {
                    TemplateFile test = new PowerPoint();
                    test.Path = temp.DirectoryName;
                    test.Name = temp.Name;
                    test.Type = type;
                    test.DisplayWord(test);
                    TemplateFile.AddElement(test);
                }
                else if (Regex.IsMatch(type, REGEX_TXT))
                {
                    TemplateFile test = new Excel();
                    test.Path = temp.DirectoryName;
                    test.Name = temp.Name;
                    test.Type = type;
                    test.DisplayWord(test);
                    TemplateFile.AddElement(test);
                }
                else if(Regex.IsMatch(type,REGEX_PDF))
                {
                    TemplateFile test = new PDF();
                    test.Path = temp.DirectoryName;
                    test.Name = temp.Name;
                    test.Type = type;
                    test.DisplayWord(test);
                    TemplateFile.AddElement(test);
                }
                else
                {
                    TemplateFile test = new UnknowFile();
                    test.Path = temp.DirectoryName;
                    test.Name = temp.Name;
                    test.Type = "unknow";
                    TemplateFile.AddElement(test);
                }
            }
            //Boucle allant affichier tout les fichier de la liste 
            foreach (TemplateFile file in TemplateFile.getInstance())
            {
                view.DisplayFile(file);
            }
        }

        ///// <summary>
        ///// Function to search all files and stock them
        ///// </summary>
        //public void GetFiles(string search)
        //{
        //    //List<TemplateFile> waitingList = new List<TemplateFile>();
        //    //waitingList = TemplateFile.getInstance();
        //    foreach(TemplateFile temp in TemplateFile.getInstance())
        //    {
        //        //string type = System.IO.Path.GetExtension(temp.Extension);
        //        if (Regex.IsMatch(temp.Type, regexWord) && temp.Name.Contains(search))
        //        {
        //            TemplateFile test = new Word();
        //            test.Path = temp.Path;
        //            test.Name = temp.Name;
        //            test.Type = temp.Type;
        //            test.DisplayWord(test);
        //            currentFiles.Add(test);
        //        }
        //        else if (Regex.IsMatch(temp.Type, regexExcel) && temp.Name.Contains(search))
        //        {
        //            TemplateFile test = new Excel();
        //            test.Path = temp.Path;
        //            test.Name = temp.Name;
        //            test.Type = temp.Type;
        //            test.DisplayWord(test);
        //            currentFiles.Add(test);
        //        }
        //        else if (Regex.IsMatch(temp.Type, regexPowerPoint) && temp.Name.Contains(search))
        //        {
        //            TemplateFile test = new PowerPoint();
        //            test.Path = temp.Path;
        //            test.Name = temp.Name;
        //            test.Type = temp.Type;
        //            test.DisplayWord(test);
        //            currentFiles.Add(test);
        //        }
        //        else if (Regex.IsMatch(temp.Type, regexTxt) && temp.Name.Contains(search))
        //        {
        //            TemplateFile test = new Excel();
        //            test.Path = temp.Path;
        //            test.Name = temp.Name;
        //            test.Type = temp.Type;
        //            test.DisplayWord(test);
        //            currentFiles.Add(test);
        //        }
        //        else if (temp.Type == "unknow" && temp.Name.Contains(search))
        //        {
        //            TemplateFile test = new UnknowFile();
        //            test.Path = temp.Path;
        //            test.Name = temp.Name;
        //            test.Type = temp.Type;
        //            test.DisplayWord(test);
        //            currentFiles.Add(test);
        //        }
        //        else if ((Regex.IsMatch(temp.Type, regexPDF) && temp.Name.Contains(search)))
        //        {
        //            TemplateFile test = new PDF();
        //            test.Path = temp.Path;
        //            test.Name = temp.Name;
        //            test.Type = temp.Type;
        //            test.DisplayWord(test);
        //            currentFiles.Add(test);
        //        }
        //    }

        //    foreach (TemplateFile file in currentFiles)
        //    {
        //        view.DisplayFile(file);
        //    }
        //}

        /// <summary>
        /// Recherche dans list de fichier courrant si mot correspondant
        /// </summary>
        /// <param name="sentence"></param>
        public void SearchWord(string[] sentence)
        {
            bool check = true;
            ResetCounter();
            fileContainWord.Clear();
            //Recherche dans tous les fichiers 
            foreach (TemplateFile file in TemplateFile.getInstance())
            {
                check = true;
                //Utilise chaque mot de la recherche
                foreach (string words in sentence)
                {

                    //Si - au début du mot
                    if (checkIfNegatif(words) == 1)
                    {
                        //Test tout les mots du fichier pour voir si correspondance
                        foreach(string word in file.wordDico.Keys)
                        {
                            //Test si correspondance entre le mot rechercher et le mot du fichier
                            if(word == words.Remove(0,1))
                            {
                                check = false;
                                file.WordLookedAtNumber = 0;
                            }
                        }
                    }

                    //Si pas mot négatif
                    if (check)
                    {
                        //Si - au début du mot
                        if (checkIfNegatif(words) == 2)
                        {
                            if (file.wordDico.ContainsKey(words.Remove(0,1)))
                            {
                                //Si oui ajout a liste des fichier contenant les fichier avec le mots
                                file.WordLookedAtNumber += file.wordDico[words.Remove(0, 1)];
                                //Si valeur se trouve dans le titre
                                if (SearchWordInTitle(words, file) == true)
                                {
                                    file.WordLookedAtNumber++;
                                }
                            }
                        }
                        //Si + au début du mot
                        else if(checkIfNegatif(words) == 3)
                        {
                            //Si valeur se trouve dans le titre
                            if (SearchWordInTitle(words, file) == true)
                            {
                                file.WordLookedAtNumber++;
                            }
                            if (file.wordDico.ContainsKey(words))
                            {
                                //Si oui ajout a liste des fichier contenant les fichier avec le mots
                                file.WordLookedAtNumber += file.wordDico[words];
                                
                            }
                        }
                    }
                }

                //Test si compteur est a 0 => ne pas afficher et que le fichier ne soit pas déjà dans la liste
                if (file.WordLookedAtNumber > 0 && !(fileContainWord.Contains(file)))
                {
                    //Ajoute fichier a la liste
                    fileContainWord.Add(file);
                }

            }
            //Appele méthode pour ordrer la liste
            OrderList();
        }

        /// <summary>
        /// Méthode pour reinit les valeurs des templates
        /// </summary>
        private void ResetCounter()
        {
            foreach(TemplateFile file in TemplateFile.getInstance())
            {
                file.WordLookedAtNumber = 0;
            }
        }

        /// <summary>
        /// Trie la liste des fichier d'après nombre d'itération 
        /// </summary>
        public void OrderList()
        {
            fileContainWordSorted = new TemplateFile[fileContainWord.Count];
            fileContainWord.ToArray();
            TemplateFile currentFile;
            for (int i = 0; i < fileContainWord.Count; i++)
            {
                for(int j= 0;j< fileContainWord.Count; j++)
                {
                    if(fileContainWordSorted[i] == null)
                    {
                        fileContainWordSorted[i] = fileContainWord[i];
                    }
                    else if(fileContainWordSorted[i].WordLookedAtNumber < fileContainWord[j].WordLookedAtNumber)
                    {
                        currentFile = fileContainWordSorted[i];
                        fileContainWordSorted[i] = fileContainWord[j];
                        fileContainWord[j] = currentFile;
                    }
                }
            }
        }

        /// <summary>
        /// Méthode cherchant dans le nom si mot rechercher existe
        /// </summary>
        /// <param name="word">mot recherché</param>
        /// <param name="file">Fichier regarder</param>
        /// <returns></returns>
        public bool SearchWordInTitle(string word,TemplateFile file)
        {
            char[] separator = new char[3];
            separator[0] = ' ';
            separator[1] = '-';
            separator[2] = '_';
            string[] temp = file.Name.Split(separator);
            if (file.Name.Contains(word))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Méthode vérifiant si un mot de recherche a un "-" au début
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public int checkIfNegatif(string word)
        {
            char[]temp = word.ToCharArray();
            if(temp[0] == '-')
            {
                return 1;
            }
            else if(temp[0] == '+')
            {
                return 2;
            }
            return 3;
        }
    }
}

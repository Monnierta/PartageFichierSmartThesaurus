﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class faisant parite de template type fichier
using Microsoft.Office.Interop.Excel;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Classe heritant de TemplateFile pour type de fichier .txt
    /// </summary>
    public class Excel : TemplateFile
    {
        public override void Open(TemplateFile file)
        {
            string path = file.Path;

            Application excel = new Application();
            Workbook wb = excel.Workbooks.Open(path + "\\" + file.Name);
        }

        /// <summary>
        /// Retourne tous les mots du fichiers
        /// </summary>
        /// <param name="file"></param>
        public override void ReturnWord(TemplateFile file)
        {
            string path = file.Path;
            string allContent = "";

            Application excel = new Application();
            Workbook wb = excel.Workbooks.Open(path + "\\" + file.Name);
            int nmbrSheets = wb.Sheets.Count;

            for (int i = 1; i < nmbrSheets + 1; i++)
            {
                Worksheet excelSheet = wb.Sheets[i];

                int rows = 10;
                int columns = 10;

                for (double x = 1; x < rows; x++)
                {
                    for (double y = 1; y < columns + 1; y++)
                    {
                        string temp = Convert.ToString(((Range)excelSheet.Cells[x, y]).Value);
                        if (temp != null)
                        {
                            allContent += temp + " ";
                        }
                    }
                }
            }

            file.AllWords = allContent.Split(' ');
            wb.Close();

            //Ferme tout les processus EXCEL en cours
            Process[] runningProcess = Process.GetProcessesByName("EXCEL");
            if(runningProcess.Length > 0)
            {
                foreach (Process process in runningProcess)
                {
                    process.Kill();
                }
            }
        }
    }
}
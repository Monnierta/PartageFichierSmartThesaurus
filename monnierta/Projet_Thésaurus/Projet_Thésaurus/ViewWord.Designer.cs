﻿namespace Projet_Thésaurus
{
    partial class ViewWord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListingWord = new System.Windows.Forms.Label();
            this.btn_AccesFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListingWord
            // 
            this.ListingWord.AutoSize = true;
            this.ListingWord.Location = new System.Drawing.Point(12, 47);
            this.ListingWord.Name = "ListingWord";
            this.ListingWord.Size = new System.Drawing.Size(0, 13);
            this.ListingWord.TabIndex = 0;
            // 
            // btn_AccesFile
            // 
            this.btn_AccesFile.Location = new System.Drawing.Point(12, 12);
            this.btn_AccesFile.Name = "btn_AccesFile";
            this.btn_AccesFile.Size = new System.Drawing.Size(75, 23);
            this.btn_AccesFile.TabIndex = 1;
            this.btn_AccesFile.Text = "Accès File";
            this.btn_AccesFile.UseVisualStyleBackColor = true;
            this.btn_AccesFile.Click += new System.EventHandler(this.btn_AccesFile_Click);
            // 
            // ViewWord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(186, 405);
            this.Controls.Add(this.btn_AccesFile);
            this.Controls.Add(this.ListingWord);
            this.Name = "ViewWord";
            this.Text = "ViewWord";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ListingWord;
        private System.Windows.Forms.Button btn_AccesFile;
    }
}
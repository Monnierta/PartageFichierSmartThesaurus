﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class faisant parite de template type fichierusing iTextSharp.text.pdf;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.Windows.Forms;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Classe heritant de TemplateFile pour type de fichier .txt
    /// </summary>
    class PDF : TemplateFile
    {
        public override void Open(TemplateFile file)
        {
        }

        /// <summary>
        /// Retourne tous les mots du fichiers
        /// </summary>
        /// <param name="file"></param>
        public override void ReturnWord(TemplateFile file)
        {
            try
            {
                using (PdfReader reader = new PdfReader(file.Path + "\\" + file.Name))
                {
                    StringBuilder text = new StringBuilder();

                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                    }
                    file.AllWords = text.ToString().Split(' ');
                }
            }
            catch
            {
                MessageBox.Show("Erreur avec un fichier pdf");
            }
        }
    }
}

﻿//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class faisant parite de template type fichier
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Classe heritant de TemplateFile pour type de fichier .docx
    /// </summary>
    public class Word : TemplateFile
    {
        public override void Open(TemplateFile file)
        {
            Application wordApp = new Application();
            Document word = wordApp.Documents.Open(file.Path + "\\" + file.Name);
        }

        /// <summary>
        /// Retourne tous les mots du fichiers
        /// </summary>
        /// <param name="file"></param>
        public override void ReturnWord(TemplateFile file)
        {
            Application wordApp = new Application();
            Document word = wordApp.Documents.Open(file.Path +"\\"+file.Name);
            List<string> theText = new List<string>();
            foreach (Range tmpRange in word.StoryRanges)
            {
                theText.Add(Convert.ToString(tmpRange.Text));
            }
            word.Close();
            wordApp.Quit();
            string text = theText[0];
            text = text.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
            file.AllWords = text.Split(' ');

            //Ferme tout les processus EXCEL en cours
            Process[] runningProcess = Process.GetProcessesByName("WORD");
            if (runningProcess.Length > 0)
            {
                foreach (Process process in runningProcess)
                {
                    process.Kill();
                }
            }
        }
    }
}

﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class principal du programme init tout les comp
using System;
using System.Windows.Forms;
using Thésaurus;

namespace Projet_Thésaurus
{
    static class Program
    {

        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ControlerFile controler = new ControlerFile();
            TemplateFile modelW = new Word();
            TemplateFile modelE = new Excel();
            TemplateFile modelP = new PowerPoint();
            TemplateFile modelT = new Txt();
            TemplateFile modelPdf = new PDF();
            modelW.SetControler(controler);
            modelE.SetControler(controler);
            modelP.SetControler(controler);
            modelT.SetControler(controler);
            modelPdf.SetControler(controler);
            ViewFile view = new ViewFile(controler);

            Application.Run(view);
        }
    }
}

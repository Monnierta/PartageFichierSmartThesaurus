﻿using REALSmartThésaurus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_Thésaurus
{
    class ModelWeb
    {
        private static ModelWeb _instance;

        private string startOfLink = "https://www.etml.ch";

        private GetLink getLink = new GetLink();

        private List<string> linkGone = new List<string>();

        private GetTextFromWord page;

        private List<GetTextFromWord> pageList = new List<GetTextFromWord>();

        private string[] tabWord ;

        internal ModelWeb Instance { get => _instance; set => _instance = value; }


        public string StartOfLink { get => startOfLink; set => startOfLink = value; }
        internal GetLink GetLink { get => getLink; set => getLink = value; }
        public List<string> LinkGone { get => linkGone; set => linkGone = value; }
        internal GetTextFromWord Page { get => page; set => page = value; }
        internal List<GetTextFromWord> PageList { get => pageList; set => pageList = value; }
        public string[] TabWord { get => TabWord1; set => TabWord1 = value; }
        public string[] TabWord1 { get => tabWord; set => tabWord = value; }

        public ModelWeb()
        {
        }

        public static ModelWeb getInstance()
        {
            if(_instance == null)
            {
                _instance = new ModelWeb();
            }
            return _instance;
        }
    }
}

﻿//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Code pour la page visuel de la venetre principale du programme
using REALSmartThésaurus;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Thésaurus;

namespace Projet_Thésaurus
{
    public partial class ViewFile : Form
    {


        // Variables pour MVC
        ControlerFile controler;
        List<Button> fileButton= new List<Button>();

        //Variable pour calcule de placement des objets
        int positionOne = 0;
        int positionTwo = 0;

        /// <summary>
        /// Constructeur perso de la classe ViewFile
        /// </summary>
        /// <param name="controler">form principale</param>
        public ViewFile(ControlerFile controler)
        {
            InitializeComponent();
            this.controler = controler;
            controler.View = this;
            controler.GetAllFiles();
        }

        /// <summary>
        /// Crée l'affichage de chaque fichier. Display them
        /// </summary>
        /// <param name="file"></param>
        public void DisplayFile(TemplateFile file)
        {
            Button button = new Button();
            button.Enabled = true;
            switch (file.Type)
            {
                case ".xlsx":
                    button.BackColor= Color.Green;
                    button.ForeColor = Color.Transparent;
                    break;
                case ".docx":
                    button.BackColor = Color.Blue;
                    break;
                case ".txt":
                    button.BackColor = Color.Gray;
                    break;
                case ".pptx":
                    button.BackColor = Color.Red;
                    break;
                case ".pdf":
                    button.BackColor = Color.Orange;
                    break;
                default :
                    button.Enabled = false;
                    button.BackColor = Color.Yellow;
                    break;
            }
             button.Text = file.Name;
             button.Width = pnl_ShowFile.Width/10;
             button.Height = pnl_ShowFile.Height/4;
            button.Left = positionOne * button.Width;
            button.Top = positionTwo * button.Height;
            button.Name = file.Name;
            button.Visible = true;
            button.Text = file.Name;
            button.Click += new EventHandler(button_Click);
             pnl_ShowFile.Controls.Add(button);
            positionOne++;
            fileButton.Add(button);
            if(positionOne >= 10)
            {
                positionOne = 0;
                positionTwo++;
            }
        }

        /// <summary>
        /// Méthode affichant pop up de fichier selectionnerr
        /// </summary>
        /// <param name="sender">Bouton ayant été cliqué</param>
        /// <param name="e"></param>
        private void button_Click(object sender, EventArgs e)
        {
            foreach (TemplateFile file in TemplateFile.getInstance())
            {
                if ( ((Button)sender).Name == file.Name)
                {
                    file.DisplayWord(file);
                    ViewWord viewWord = new ViewWord();
                    viewWord.Show();
                    viewWord.ShowWord(file);
                }
            }
        }

        /// <summary>
        /// Méthode servant a vider l'interface des anciens fichiers
        /// </summary>
        public void CleanFile()
        {
            positionOne = 0;
            positionTwo = 0;
            foreach (Button obj in fileButton)
            {
                pnl_ShowFile.Controls.Remove(obj);
                obj.Dispose();
            }
            fileButton.Clear();
        }

        /// <summary>
        /// Retourne mot rechercher
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public string[] GetWordsSearch(string word)
        {
            string sentence = searchBarreWord.Text;
            string[]temp =  sentence.Split(' ');
            return temp;
        }

        /// <summary>
        /// Execute la recherche de mot dans les fichiers déjà load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchWord_Click(object sender, EventArgs e)
        {
            CleanFile();
            ControlerFile.fileContainWord.Clear();
            if (searchBarreWord.Text != "")
            {
                //Reset la liste pour la réutiliser
                if (ControlerFile.fileContainWordSorted != null)
                {
                    for (int i = 0; i < ControlerFile.fileContainWordSorted.Length; i++)
                    {
                        ControlerFile.fileContainWordSorted[i] = null;
                    }
                }

                //Recherche dans la liste
                controler.SearchWord(GetWordsSearch(searchBarreWord.Text));

                //Affihce chaque fichiers de la lite trié
                foreach (TemplateFile file in ControlerFile.fileContainWordSorted)
                {
                    DisplayFile(file);
                }
            }
            else
            {
                foreach (TemplateFile file in TemplateFile.getInstance())
                {
                    DisplayFile(file);
                }
            }
        }

        /// <summary>
        /// Evenement changeant le répértoire a fouiller
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changeRepertoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fileDialog = new FolderBrowserDialog();
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                controler.RepertoryAcces = fileDialog.SelectedPath;
            }
        }

        /// <summary>
        /// Refresh la  liste de fichier du programme de façon a avoir les fichiers les plus récent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CleanFile();
            TemplateFile.getInstance().Clear();
            ControlerFile.fileContainWord.Clear();
            ControlerFile.fileContainWordSorted = null;
            controler.GetAllFiles();
        }

        /// <summary>
        /// Méthode affichant boite d'information d'aide sur le programme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpBox help = new HelpBox();
            help.Show();
        }

        private void ViewFile_Load(object sender, EventArgs e)
        {

        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void serachOnWebsiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModelWeb.getInstance().LinkGone = ModelWeb.getInstance().GetLink.getLinks(ModelWeb.getInstance().StartOfLink);

            foreach(string link in ModelWeb.getInstance().LinkGone)
            {
                ModelWeb.getInstance().Page = new GetTextFromWord(ModelWeb.getInstance().TabWord, ModelWeb.getInstance().StartOfLink + link);
                ModelWeb.getInstance().Page.WordPerPage = (ModelWeb.getInstance().Page.GetTextInHtml(ModelWeb.getInstance().Page.Link));
                ModelWeb.getInstance().PageList.Add(ModelWeb.getInstance().Page);
            }
        }
    }
}

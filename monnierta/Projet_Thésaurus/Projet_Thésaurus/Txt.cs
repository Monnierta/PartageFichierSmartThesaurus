﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class faisant parite de template type fichier
using System;
using System.IO;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Classe heritant de TemplateFile pour type de fichier .txt
    /// </summary>
    public class Txt : TemplateFile
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        public override void Open(TemplateFile file)
        {
           //Cannont be open 
        }

        /// <summary>
        /// Retourne tous les mots du fichiers
        /// </summary>
        /// <param name="file"></param>
        public override void ReturnWord(TemplateFile file)
        {
            StreamReader reader = new StreamReader(file.Path+"\\" + file.Name);
            String text = reader.ReadToEnd();
            text = text.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
            file.AllWords = text.Split(' ');
        }
    }
}
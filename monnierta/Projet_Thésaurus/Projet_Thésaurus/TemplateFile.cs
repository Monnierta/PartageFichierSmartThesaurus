﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Template pour type de fichier lisible
using System;
using System.Collections.Generic;
using Thésaurus;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Class template pour file
    /// </summary>
    abstract public class TemplateFile
    {
        //Element list contenant tout les fichiers
        private static List<TemplateFile> files = new List<TemplateFile>();

        // déclaration des variables
        public Dictionary<string, int> wordDico = new Dictionary<string, int>();
        protected ControlerFile controler;
        private string _path;
        private string _name;
        private string _type;
        private int _wordLookedAtNumber = 0;
        private bool _readed = false;
        private DateTime _lastModified;
        private string[] allWords = new string[30000];
        private bool check = true;

        // Getter Setter
        public string Path { get => _path; set => _path = value; }
        public string Name { get => _name; set => _name = value; }
        public string Type { get => _type; set => _type = value; }
        public DateTime LastModified { get => _lastModified; set => _lastModified = value; }
        public string[] AllWords { get => allWords; set => allWords = value; }
        public bool Readed { get => _readed; set => _readed = value; }
        public int WordLookedAtNumber { get => _wordLookedAtNumber; set => _wordLookedAtNumber = value; }

        //Méthode étant hériter
        abstract public void ReturnWord(TemplateFile file);
        abstract public void Open(TemplateFile file);

        /// <summary>
        /// Constructeur perso (création de fichier)
        /// </summary>
        /// <param name="path">chemin du fichier</param>
        public TemplateFile(string path)
        {
            check = true;
            files = new List<TemplateFile>();
            _path = path;
            _name = System.IO.Path.GetFileNameWithoutExtension(path);
            _type = System.IO.Path.GetExtension(path);
            _lastModified = System.IO.File.GetLastWriteTime(path);
        }

        /// <summary>
        /// Méthode retournant instance courrent de la liste
        /// </summary>
        /// <returns></returns>
        public static List<TemplateFile> getInstance()
        {
            if(files == null)
            {
                return new List<TemplateFile>();
            }
            return files;
        }

        /// <summary>
        /// Constructeur de base
        /// </summary>
        public TemplateFile()
        {
            _path = null;
        }

        /// <summary>
        /// Ajout nouveau élément file a list des file utilisé en global
        /// </summary>
        /// <param name="file"></param>
        public static void AddElement(TemplateFile file)
        {
            files.Add(file);
        }

        /// <summary>
        /// Constructeur pour MVC
        /// </summary>
        /// <param name="controler">chemin du fichier</param>
        public void SetControler(ControlerFile controler)
        {
            this.controler = controler;
        }

        /// <summary>
        /// Méthode servant a ajouter chaque mot du fichier a un dico 
        /// </summary>
        /// <param name="file">fichier utiliser</param>
        public void DisplayWord(TemplateFile file)
        {
            ReturnWord(file);
            if(!Readed)
            {
                foreach (string word in AllWords)
                {
                    if(word != null)
                    {
                        if (wordDico.Count >= 1)
                        {
                            for (int i = 0; i < AllWords.Length - 1; i++)
                            {
                                if (wordDico.ContainsKey(word))
                                {
                                    wordDico[word]++;
                                    break;
                                }
                                else
                                {
                                    wordDico.Add(word, 1);
                                    break;
                                }
                            }
                        }
                        else if(word == Name && check)
                        {
                            check = false;
                            if (wordDico.ContainsKey(word))
                            {
                                wordDico[word]++;
                                break;
                            }
                            else
                            {
                                wordDico.Add(word, 1);
                                break;
                            }
                        }
                        else
                        {
                            wordDico.Add(word, 1);
                        }
                    }
                }
                Readed = true;
            }
        }
    }
}

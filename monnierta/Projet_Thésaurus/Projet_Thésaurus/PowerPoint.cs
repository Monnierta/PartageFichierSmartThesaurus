﻿//ETML
//Auteur : Len Henchoz 
//Date : 27.02.18
//Description : Class faisant parite de template type fichier
using Microsoft.Office.Core;
using System.Diagnostics;

namespace Projet_Thésaurus
{
    /// <summary>
    /// Classe heritant de TemplateFile pour type de fichier .txt
    /// </summary>
    public class PowerPoint : TemplateFile
    {
        public override void Open(TemplateFile file)
        {
            Microsoft.Office.Interop.PowerPoint.Application PowerPoint_App = new Microsoft.Office.Interop.PowerPoint.Application();
            Microsoft.Office.Interop.PowerPoint.Presentations multi_presentations = PowerPoint_App.Presentations;
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = multi_presentations.Open(file.Path + "\\" + file.Name, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);
        }

        /// <summary>
        /// Retourne tous les mots du fichiers
        /// </summary>
        /// <param name="file"></param>
        public override void ReturnWord(TemplateFile file)
        {
            Microsoft.Office.Interop.PowerPoint.Application PowerPoint_App = new Microsoft.Office.Interop.PowerPoint.Application();
            Microsoft.Office.Interop.PowerPoint.Presentations multi_presentations = PowerPoint_App.Presentations;
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = multi_presentations.Open(file.Path+"\\" + file.Name, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);

            int nbrSlide = presentation.Slides.Count;
            string presentation_textforParent = "";
            for (int i = 0; i < nbrSlide; i++)
            {
                foreach (var item in presentation.Slides[i + 1].Shapes)
                {
                    var shape = (Microsoft.Office.Interop.PowerPoint.Shape)item;
                    if (shape.HasTextFrame == MsoTriState.msoTrue)
                    {
                        if (shape.TextFrame.HasText == MsoTriState.msoTrue)
                        {
                            var textRange = shape.TextFrame.TextRange;
                            var text = textRange.Text;

                            presentation_textforParent += text + " ";
                        }
                    }
                }
            }
            file.AllWords = presentation_textforParent.Split(' ');

            //Ferme tout les processus EXCEL en cours
            Process[] runningProcess = Process.GetProcessesByName("POWERPNT");
            if (runningProcess.Length > 0)
            {
                foreach (Process process in runningProcess)
                {
                    process.Kill();
                }
            }
        }
    }
}
﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using word = Microsoft.Office.Interop.Word;
using System.Net;

namespace REALSmartThésaurus
{
    class Program
    {
        /// <summary>
        /// liste des mots triés
        /// </summary>


        static string startOfLink = "https://www.etml.ch";
        static GetLink getLink = new GetLink();
        

        static List<string> linkGone = new List<string>();

        static GetTextFromWord page;

        static List<GetTextFromWord> pageList = new List<GetTextFromWord>();

        static string[] tabWord;
            
        static void Main(string[] args)
        {
            linkGone = getLink.getLinks(startOfLink);


            foreach (string link in linkGone)
            {
               

                page = new GetTextFromWord(tabWord, startOfLink + link);
                page.WordPerPage = (page.GetTextInHtml(page.Link));
                pageList.Add(page);
            }

            foreach (GetTextFromWord Page in pageList)
            {
                foreach (string wordArray in Page.WordPerPage)
                {
                    Console.WriteLine(wordArray);
                    
                    //TODO faire plus de verif pour les mots 
                    //mettre dans l'autre program

                }
            }
        }
    }
}

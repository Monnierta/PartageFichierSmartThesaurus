﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REALSmartThésaurus
{
    class SingletonWord
    {
        private static object syncRot = new Object();
        private static volatile SingletonWord instance;
        private SingletonWord()
        {

        }
        public static SingletonWord Instance()
        {
           
            if (instance == null)
            {
                lock (syncRot)
                {
                    instance = new SingletonWord();
                }

            }
            return instance;
        }
    }
}
